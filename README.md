### Flint2html ###
## Description ##

Flint2html is a script that converts [flint](https://github.com/facebook)'s output in html format. 

If is possible, this script shows all errors contained in a flint's output file, in a table inside the html file. Doing this, Flint2html requires, in *input*, the file that contains all errors to report in the table and its relative **prefix** (that can be specified with the parameter *-p* ), that will be the word with wich every path start with.

Flint2html reads default from stdin and writes on stdout, but users can specify other parameters:

* *-o* : That allows the user to choose an **output file**.
* *-t* : That allows the user to choose the **title** that will be showed in html's page.
* *-b* : That allows the user to choose which will be the **base href** (the script will create all link to the file(s)'s URL).
* *-i* : An other way to select the **input file**.