#!/bin/bash

trap 'rm -rf ${tmpdir}' 0

function usage(){
  echo -e "Usage: $(basename $0) -p prefix [-b base_href] [-o output_file] [-t title] [-i input_file]" 1>&2
}

while getopts 'p:b:o:t:i:' o; do
  case "${o}" in
    p)
      prefix=$(echo "${OPTARG}" | tr -d \")
      ;;
    b)
      base_href=$(echo "${OPTARG}" | tr -d \")
      ;;
    o)
      output_file=$(echo "${OPTARG}" | tr -d \")
      ;;
    t)
      input_title=$(echo "${OPTARG}" | tr -d \")
      ;;
    i)
      input_file=$(echo "${OPTARG}" | tr -d \")
      ;;
  esac
done

if [ "${prefix}" = "" ]; then
  usage
  exit 1
fi

if [ "${base_href}" != "" ]; then

  if echo "${base_href}" | grep -q "^https\?"; then

    host=$(echo ${base_href} | sed -e 's#^https\?://\([^/]\+\)/.*$#\1#')

    if echo ${host} | grep -q "bitbucket"; then
      line_prefix='#cl-'   
    elif echo ${host} | grep -q "github"; then
      line_prefix='#L'

    fi

  base_href_line="<base href='${base_href}' target='_blank' />"

  else

    directory=$(readlink -f ${base_href})

    if [ -d "${directory}" ]; then

      base_href="file://${directory}/"
      base_href_line="<base href='${base_href}' target='_blank' />"

    fi

  fi

fi

if [ "${input_title}" = "" ]; then

  title_line="<title>Flint2html</title>"

else

  h1_line="<h1 align='center'>${input_title}</h1>"
  title_line="<title>${input_title}</title>"

fi

if [ "${input_file}" != "" ]; then

  exec < ${input_file}

fi

if [ "${output_file}" != "" ]; then

  exec > ${output_file}

fi

cat <<EOF
<html>
  <head>
    <link href='http://fonts.googleapis.com/css?family=Orbitron:900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    ${title_line}
    ${base_href_line}
    <style>
      table{
      font-family: 'Lato', sans-serif;
      }
      body{
      font-family: 'Orbitron', sans-serif;
      font-size:xx-large;
      }
      a{
      color:green;
      text-decoration:none;
      }
      a:active{
      color:maroon;
      }
      a:hover {
      color:red;
      }
      a:focus:hover{
      color:lime;
      }
      td a:active{
      color:maroon;
      }
      td.line {
      font-family: courier;
      color: grey;
      font-size: 70%;
      }
      td.code {
      font-family: courier;
      white-space: pre-wrap;
      font-size: 70%;
      }
      td.highlight {
      background-color: Yellow;
      font-family: courier;
      white-space: pre-wrap;
      font-size: 70%;
      }
    </style>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'></script
  </head>
  <body>
    ${h1_line}
    <table width="100%" align="center" bordercolor="blue" border="2">
      <tr>
        <td align='center'>FILE(LINE)</td>
        <td align='center'>MESSAGE</td>
      </tr>
EOF

tmpdir=$(mktemp -d)
no_link=0

if [ "${base_href}" = "" ] && ! [ -d "${prefix}/${file_name}" ]; then

  no_link=1

fi

while read line; do

  echo ${line} | grep -q "^${prefix}"
  if [ $? -ne 0 ]; then continue; fi

  file_name=$(echo "${line}" | sed -e 's:^'${prefix}'/\([^(]\+\)(.*$:\1:')
  line_number=$(echo "${line}" | sed -e 's:.*[(]\([0-9]\+\)).*$:\1:')
  message=$(set -o noglob; echo "${line}" | sed -e 's/.\+([0-9]\+).\? \+\(.\+\).*$/\1/' | sed -e 's:<:\&lt;:g' -e 's:>:\&gt;:g' | sed -e 's#\(http://[^ ]\+\)#<a href="\1">\1</a>#g')
  line_info="${line_prefix}${line_number}"

    if [ ${line_number} -lt 3 ]; then
      start_line=1
    else
      start_line=$((${line_number}-2))
    fi

  end_line=$((${line_number}+2))

  if echo "${base_href}" | grep -q "^https\?"; then

    href_line="<p><a href='${file_name}${line_info}'>${file_name}(${line_number})</a></p>"
    previous_file=""
    file=$(basename "${file_name}")
    curl_error_control=0

    if [ ${curl_error_control} -eq 0 ]; then

      if echo "${host}" | grep -q "bitbucket"; then

        curl_prefix=$(echo "${base_href}" | awk -F/ '{ print $4"/"$5"/raw/"$7 }')      

        if [ "${file_name}" != "${previous_file}" ]; then

          $(curl -s -f "https://api.bitbucket.org/1.0/repositories/${curl_prefix}/${file_name}" > "${tmpdir}/${file}")
          curl_error_control=$?
          previous_file="${file_name}"

        fi

      elif echo "${host}" | grep -q "github"; then

        curl_prefix=$(echo "${base_href}" | awk -F/ '{ print $4"/"$5"/contents/"}')

        if [ "${file_name}" != "${previous_file}" ]; then

          $(curl -s -f -H 'Accept: application/vnd.github.raw'  "https://api.github.com/repos/${curl_prefix}${file_name}?ref=master" > "${tmpdir}/${file}")
          curl_error_control=$?
          previous_file="${file_name}"

        fi

      fi

      declare -a code_line

      for i in $(seq ${start_line} 1 ${end_line});

        do

          if [ ${i} -eq ${line_number} ]; then

            code_line[${i}]="<tr><td class='line'>${i}</td><td class='highlight'>"$(cat "${tmpdir}/${file}" | sed -n ${i}"p" | sed -e 's:<:\&lt;:g')"</td></tr>";

          else

            code_line[${i}]="<tr><td class='line'>${i}</td><td class='code'>"$(cat "${tmpdir}/${file}" | sed -n ${i}"p" | sed -e 's:<:\&lt;:g')"</td></tr>";

          fi

        done

    fi

  elif [ -d "${directory}" ] || echo "${base_href}" | grep -q "^file://"; then

    href_line="<p><a href='${prefix}/${file_name}'>${file_name}(${line_number})</a></p>"

    if [ -r "${directory}/${file_name}" ]; then

      href_line="<p><a href='${file_name}'>${file_name}(${line_number})</a></p>"

      declare -a code_line

      printf -v code_line[$((${start_line}-1))] '<table>\n';

      for i in $(seq ${start_line} 1 ${end_line});

        do

          if [ ${i} -eq ${line_number} ]; then

            code_line[${i}]="<tr><td class='line'>${i}</td><td class='highlight'>"$(cat "${directory}/${file_name}" | sed -n ${i}"p" | sed -e 's:<:\&lt;:g')"</td></tr>";

          else

            code_line[${i}]="<tr><td class='line'>${i}</td><td class='code'>"$(cat "${directory}/${file_name}" | sed -n ${i}"p" | sed -e 's:<:\&lt;:g')"</td></tr>";

          fi

        done

    fi
      
  elif [ -r "${prefix}/${file_name}" ]; then

    href_line="<p><a href='${prefix}/${file_name}'>${file_name}(${line_number})</a></p>"

    declare -a code_line

    for i in $(seq ${start_line} 1 ${end_line});

      do

        if [ ${i} -eq ${line_number} ]; then

          code_line[${i}]="<tr><td class='line'>${i}</td><td class='highlight'>"$(cat "${prefix}/${file_name}" | sed -n ${i}"p" | sed -e 's:<:\&lt;:g')"</td></tr>";

        else

          code_line[${i}]="<tr><td class='line'>${i}</td><td class='code'>"$(cat "${prefix}/${file_name}" | sed -n ${i}"p" | sed -e 's:<:\&lt;:g')"</td></tr>";

        fi

      done

  else

    no_link=1

  fi

if [ ${no_link} -eq 1 ]; then

    href_line="<p>${file_name}(${line_number})</p>"

fi

cat <<EOF
      <tr>
        <td>
          ${href_line}
          <table>
            ${code_line[*]}
          </table>
        </td>
        <td>
          ${message}
        </td>
      </tr>
EOF

unset code_line

done

cat <<EOF
    </table>
  </body>
</html>
EOF
